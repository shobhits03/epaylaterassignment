# answers to questions


###### Q1 How long did you spend on the coding test? What would you add to your solution if you spent more time on it? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

I spent around a day for this project although I could have done this task in 2-3 hours but I really wanted to impliment dagger2 since a long time, Thanks to this assignment I got the chance to clearify the concept of dagger2. If given more time I would have added data binding concept in mvvm patern which would have made life more easier.

###### Q2 What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
Acc to me Rx java and RxAndroid is the most useful feature recently added. Multithreading became easier for me.
```java
private void handleLoadMore(ProgressDialog progressDialog) {
        Observable.create((Observable.OnSubscribe<List<PassDataModel>>) subscriber -> {
            List<PassDataModel> dataModels = new ArrayList<>();
            dataModels.clear();
            int next_item_to_populate = getNextItemTorequest();
            for (int i = last_item_populated; i < next_item_to_populate; i++) {
                dataModels.add(list.get(i));
            }
            last_item_populated = next_item_to_populate;
            subscriber.onNext(dataModels);
        }).doOnCompleted(() -> progressDialog.dismiss()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PassDataModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<PassDataModel> passDataModels) {
                        passbookAdapterThisYear.appendData(passDataModels);
                        progressDialog.dismiss();
                    }
                });
    }

```
###### Q3What is your favourite framework / library / package that you love but couldn't use in the task? What do you like about it so much?
My favourite is Dagger2. I have heard about this library around few times but never got a chance to impliment. The best thing about this library that it eliminates the boilerplate code. Although at start I thought that it is even lengthy than the current code but when the projects get big, it really do help plus the popularity of this library. Thanks to this assignment I have finally implimented it.  

###### Q4 What great new thing you learnt about in the past year and what are you looking forward to learn more about over the next year?
In the past year and till now I learnt about reactive programming, retrofit, I have impliment around all kinds of payment gateways like paytm, phonepe, amazonPay,freeCharge, Airtel money,Payu,razorpay. Adding more I have learnt about caching like LRU cache 3rd party integration like facebook,firebase google analytics.Moving forward I would like to get knowledge of livedata, reactive programming and dagger2.

###### Q5 How would you track down a performance issue in production? Have you ever had to do this?
I personally use firebase crashlytics and fabric for tracking down the performance issues in production.Plus we test the app on around 10 types of devices with differnt screensizes and pixel density so that I can delive a reliable product .My Current App is Coolwinks.

###### Q6 How would you improve the APIs that you just used?
The APIs were preety good but while working on the project sometime I didn't got the response or the response time was high. So I will try to reduce th# answers to questions


###### Q1 How long did you spend on the coding test? What would you add to your solution if you spent more time on it? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

I spent around a day for this project although I could have done this task in 2-3 hours but I really wanted to impliment dagger2 since a long time, Thanks to this assignment I got the chance to clearify the concept of dagger2. If given more time I would have added data binding concept in mvvm patern which would have made life more easier.

###### Q2 What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
Acc to me Rx java and RxAndroid is the most useful feature recently added. Multithreading became easier for me.
```java
private void handleLoadMore(ProgressDialog progressDialog) {
        Observable.create((Observable.OnSubscribe<List<PassDataModel>>) subscriber -> {
            List<PassDataModel> dataModels = new ArrayList<>();
            dataModels.clear();
            int next_item_to_populate = getNextItemTorequest();
            for (int i = last_item_populated; i < next_item_to_populate; i++) {
                dataModels.add(list.get(i));
            }
            last_item_populated = next_item_to_populate;
            subscriber.onNext(dataModels);
        }).doOnCompleted(() -> progressDialog.dismiss()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PassDataModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<PassDataModel> passDataModels) {
                        passbookAdapterThisYear.appendData(passDataModels);
                        progressDialog.dismiss();
                    }
                });
    }

```
###### Q3What is your favourite framework / library / package that you love but couldn't use in the task? What do you like about it so much?
My favourite is Dagger2. I have heard about this library around few times but never got a chance to impliment. The best thing about this library that it eliminates the boilerplate code. Although at start I thought that it is even lengthy than the current code but when the projects get big, it really do help plus the popularity of this library. Thanks to this assignment I have finally implimented it.  

###### Q4 What great new thing you learnt about in the past year and what are you looking forward to learn more about over the next year?
In the past year and till now I learnt about reactive programming, retrofit, I have impliment around all kinds of payment gateways like paytm, phonepe, amazonPay,freeCharge, Airtel money,Payu,razorpay. Adding more I have learnt about caching like LRU cache 3rd party integration like facebook,firebase google analytics.Moving forward I would like to get knowledge of livedata, reactive programming and dagger2.

###### Q5 How would you track down a performance issue in production? Have you ever had to do this?
I personally use firebase crashlytics and fabric for tracking down the performance issues in production.Plus we test the app on around 10 types of devices with differnt screensizes and pixel density so that I can delive a reliable product .My Current App is Coolwinks.

###### Q6 How would you improve the APIs that you just used?
The APIs were preety good but while working on the project sometime I didn't got the response or the response time was high. So I will try to reduce the response time of the api.

###### Q7 Please describe yourself in JSON format.
Pretty Good. I have parsed many json manually before I started using GSON for the json Conversion.

###### Q8 What is the meaning of life?
Life is something that we can make out of it. Life for me is ups and downs, straight roads and speedbumps, success and failure.e response time of the api.

###### Q7 Please describe yourself in JSON format.
Pretty Good. I have parsed many json manually before I started using GSON for the json Conversion.

###### Q8 What is the meaning of life?
Life is something that we can make out of it. Life for me is ups and downs, straight roads and speedbumps, success and failure.